import 'package:flutter/material.dart';
import 'package:todo/screens/screens.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings routes) {

    final dynamic args = routes.arguments;

    switch (routes.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => HomeScreen());
      case '/add_task':
        return MaterialPageRoute(builder: (context) => TaskAddScreen());
      case '/task_detail':
        return MaterialPageRoute(
            builder: (context) => (TaskDetail(
                  task: args,
                )));
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('error'),
          centerTitle: true,
        ),
        body: Center(
          child: Text('Page Not Found'),
        ),
      );
    });
  }
}
