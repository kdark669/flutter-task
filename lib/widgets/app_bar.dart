import 'package:flutter/material.dart';
import 'package:todo/styles/palette.dart';
import 'package:todo/styles/vars.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double _preferredSize = 55.0;
  final Text title;
  final bool isSecond;

  const MyAppBar({Key key,this.title, this.isSecond})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.light,
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      automaticallyImplyLeading: true,
      centerTitle: false,
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      title: title,
      actions: [
        IconButton(
          onPressed: () {},
          color: Palette.dark,
          iconSize: Font.lg,
          hoverColor: Palette.primary,
          icon: Icon(Icons.person),
        )
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_preferredSize);
}
