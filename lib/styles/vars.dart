
class Padding {
  static const double xl = 40;
  static const double lg = 30;
  static const double md = 20;
  static const double sm = 10;
  static const double xs = 5;
}

class Font {
  static const double xl = 40;
  static const double lg = 28;
  static const double md = 18;
  static const double sm = 12;
}