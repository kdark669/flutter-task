import 'package:flutter/material.dart';
import 'package:todo/styles/vars.dart';

class Layout {
  static const title = TextStyle(
      fontSize: Font.lg, color: Colors.black87, fontWeight: FontWeight.bold);

  static const subTitle = TextStyle(
      fontSize: Font.md, color: Colors.black87);

  //display-layout
  static const displayBetween = MainAxisAlignment.spaceBetween;
  static const displayEvenly = MainAxisAlignment.spaceEvenly;
  static const displayCenter = MainAxisAlignment.center;
  static const displayEnd = MainAxisAlignment.end;
  static const displayStart = MainAxisAlignment.start;

}
