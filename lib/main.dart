import 'package:flutter/material.dart';
import 'package:todo/styles/palette.dart';
import 'package:todo/routes/routes.dart';
import 'package:todo/screens/screens.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Task ',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Palette.scaffold
      ),
      home: HomeScreen(),
      //generate routes
      onGenerateRoute: Routes.generateRoute ,
    );
  }
}
