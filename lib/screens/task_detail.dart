import 'package:flutter/material.dart';
import 'package:todo/widgets/app_bar.dart';

class TaskDetail extends StatelessWidget {
  final task;

  const TaskDetail({Key key, this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        isSecond: true,
      ),
      body: Center(
        child: Text(task) ,
      ),
    );
  }
}
