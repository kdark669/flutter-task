import 'package:flutter/material.dart';
import 'package:todo/styles/layout.dart';
import 'package:todo/styles/palette.dart';
import 'package:todo/widgets/app_bar.dart';

class TaskAddScreen extends StatefulWidget {
  @override
  _TaskAddScreenState createState() => _TaskAddScreenState();
}

class _TaskAddScreenState extends State<TaskAddScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Palette.scaffold,
        appBar: MyAppBar(
          isSecond: true,
          title: Text('Add Task', style: Layout.subTitle),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Text('this add task screen', style: Layout.title),
              RaisedButton(
                child: Text('Go Home', style: Layout.title),
                onPressed: () {
                  Navigator.of(context).pop('/');
                },
              )
            ],
          ),
        )
    );
  }
}
