import 'package:flutter/material.dart';
import 'package:todo/styles/layout.dart';
import 'package:todo/styles/palette.dart';
import 'package:todo/widgets/widget.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Palette.scaffold,
        appBar: MyAppBar(
          title: Text('Task', style: Layout.title),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Text('hello world', style: Layout.title),
              RaisedButton(
                child: Text('Add Task', style: Layout.title),
                highlightColor: Colors.blue,
                onPressed: () {
                  Navigator.of(context).pushNamed('/add_task');
                },
              ),
              RaisedButton(
                child: Text('Task Detail', style: Layout.title),
                onPressed: () {
                  Navigator.of(context).pushNamed('/task_detail', arguments: " this is task details ");
                },
              )
            ],
          ),
        ));
  }
}
